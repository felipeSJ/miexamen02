package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

    private Tabletop tabletop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        tabletop = getIntent().getParcelableExtra("tableTop");
        setFragment(tabletop);
    }

    public void setFragment(Tabletop item) {
        Bundle bundle = new Bundle();
        TabletopFragment details = new TabletopFragment();
        bundle.putParcelable("currentItem", item);
        details.setArguments(bundle);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.tabletop_details, details);
        ft.commit();
    }
}