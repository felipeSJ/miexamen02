package cr.ac.ucr.ecci.cql.miexamen02;

import java.util.List;

public interface MainActivityView {
    // Mostrar el progreso en la UI del avance de la tarea a realizar
    void showProgress();
    // Esconder el indicador de progreso de la UI
    void hideProgress();
    // Mostrar los items de la lista en la UI
    void setItems(List<Tabletop> items);
}
