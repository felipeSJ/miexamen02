package cr.ac.ucr.ecci.cql.miexamen02;

public interface IServiceDataSource {
    void obtainItems() throws CantRetrieveItemsException;
}
