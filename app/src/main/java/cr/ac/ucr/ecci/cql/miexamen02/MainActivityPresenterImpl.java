package cr.ac.ucr.ecci.cql.miexamen02;

import java.util.List;

public class MainActivityPresenterImpl implements MainActivityPresenter, TabletopInteractor.OnFinishedListener{

    private MainActivityView mMainActivityView;
    private TabletopInteractor tabletopInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView){
        this.mMainActivityView = mainActivityView;
        // Capa de negocios (Interactor)
        this.tabletopInteractor = new TabletopInteractorImpl();
        //this.personasInteractor.getItems(this);
    }

    @Override
    public void onResume(){
        if(mMainActivityView!=null){
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        tabletopInteractor.getItems(this);
    }

    @Override
    public void onItemClicked(Tabletop item) {

    }

    // Evento de clic en la lista
    /*@Override
    public void onItemClicked(Persona item){
        if(mMainActivityView!=null){
            mMainActivityView.setFragment(item);
        }
    }*/

    @Override
    public void onDestroy(){
        mMainActivityView=null;
    }

    @Override
    public void onFinished(List<Tabletop> items){
        if(mMainActivityView!=null){
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }
    // Retornar la vista
    public MainActivityView getMainView(){
        return mMainActivityView;
    }
}
