package cr.ac.ucr.ecci.cql.miexamen02;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IServiceDataSourceImpl implements IServiceDataSource{
    private static final String URL_REST = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop14.json";
    private static final String TAG_T = "Tabletop";
    public TaskServicioREST task;
    private List<Tabletop> items;
    private TabletopInteractor.OnFinishedListener mListener;

    public IServiceDataSourceImpl(final TabletopInteractor.OnFinishedListener listener){
        mListener = listener;
        task = new TaskServicioREST();
    }

    @Override
    public void obtainItems() throws CantRetrieveItemsException {
        try {
            task.execute(URL_REST);

        } catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
    }

    public String getArrayFromJson(String json){
        int start = json.indexOf("[");
        int end = json.length() - 1;
        return json.substring(start, end);
    }

    public void postExecuteHelper(String json){
        String data = getArrayFromJson(json);
        Gson gson = new Gson();
        Tabletop[] jsonObject = gson.fromJson(data, Tabletop[].class);
        items = new ArrayList<>(Arrays.asList(jsonObject));
        mListener.onFinished(items);
    }


    // Clase para la tarea asincronica en Servicio REST
    public class TaskServicioREST extends AsyncTask<String, Float, String> {

        private ProgressDialog mDialog;
        private String aux;
        public TaskServicioREST(){
            mDialog = MainActivity.getmDialog();
        }

        @Override
        protected void onPreExecute() {
            mDialog.setProgress(0);
        }

        @Override
        protected String doInBackground(String... urls) {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {}
                // publicamos el progreso de la tarea
                publishProgress(i/100f);
            }
            return loadContentFromNetwork(urls[0]);
        }

        protected void onProgressUpdate (Float... values) {
            // actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }

        protected void onPostExecute(String bytes) {
            // Cuando la tarea termina cerramos la barra de progreso
            mDialog.dismiss();
            postExecuteHelper(aux);
        }

        // metodo para bajar el contenido
        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder();
                String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }
                aux = strBuilder.toString();
                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_T, e.getMessage());
            }
            return null;
        }
    }
}