package cr.ac.ucr.ecci.cql.miexamen02;

public interface MainActivityPresenter {
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(Tabletop item);
    // destruir
    void onDestroy();
}
