package cr.ac.ucr.ecci.cql.miexamen02;

import android.os.Handler;

public class TabletopInteractorImpl implements TabletopInteractor {
    private IServiceDataSource iServiceDataSource;

    @Override public void getItems(final OnFinishedListener listener) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                //Pasamos el listener para que se escuche hasta que el asynctask sea completado
                iServiceDataSource = new IServiceDataSourceImpl(listener);
                try {
                    // obtenemos los items
                    iServiceDataSource.obtainItems();
                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }
            }
        }, 0);
    }
}
