package cr.ac.ucr.ecci.cql.miexamen02;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener {

    private ListView mListView;
    private MainActivityPresenter mMainActivityPresenter;
    private static ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Ejecutando tarea asincrónica...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();

        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Tabletop tabletop = (Tabletop) mListView.getItemAtPosition(pos);
        Intent detailActivity = new Intent(MainActivity.this, DetailActivity.class);
        detailActivity.putExtra("tableTop", tabletop);
        startActivity(detailActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override
    public void showProgress() {
        mListView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        mListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setItems(List<Tabletop> items) {
        LazyAdapter mAdapter = new LazyAdapter(items, this);
        mListView.setAdapter(mAdapter);
    }

    public static ProgressDialog getmDialog(){
        return mDialog;
    }
}