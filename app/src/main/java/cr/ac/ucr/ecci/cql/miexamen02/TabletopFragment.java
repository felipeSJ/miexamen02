package cr.ac.ucr.ecci.cql.miexamen02;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TabletopFragment extends Fragment {
    private Tabletop item;

    public TabletopFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public static TabletopFragment newInstance(int index) {
        TabletopFragment f = new TabletopFragment();
        // Provee el index como argumento para mostrar el detalle de datos.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            this.item = bundle.getParcelable("currentItem");
        }
        return inflater.inflate(R.layout.fragment_tabletop, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.setTextValues(view);
    }

    public void setTextValues(View view){
        TextView id = view.findViewById(R.id.itemId);
        id.setText(item.getIdentificacion());

        TextView nombre = view.findViewById(R.id.itemNombre);
        nombre.setText(item.getNombre());

        TextView año = view.findViewById(R.id.itemAño);
        año.setText(item.getYear().toString());

        TextView pub = view.findViewById(R.id.itemPublisher);
        pub.setText(item.getPublisher());
    }
}


