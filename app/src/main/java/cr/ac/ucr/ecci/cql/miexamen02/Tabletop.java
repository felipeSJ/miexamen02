package cr.ac.ucr.ecci.cql.miexamen02;

import android.os.Parcel;
import android.os.Parcelable;

public class Tabletop implements Parcelable{
    private String identificacion;
    private String nombre;
    private Integer year;
    private String publisher;

    public Tabletop(String identificacion, String nombre, Integer year, String publisher) {
        this.setIdentificacion(identificacion);
        this.setNombre(nombre);
        this.setYear(year);
        this.setPublisher(publisher);
    }

    protected Tabletop(Parcel in){
        identificacion = in.readString();
        nombre = in.readString();
        year = in.readInt();
        publisher = in.readString();
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public static final Parcelable.Creator<Tabletop> CREATOR = new Parcelable.Creator<Tabletop>() {
        @Override
        public Tabletop createFromParcel(Parcel in) {
            return new Tabletop(in);
        }

        @Override
        public Tabletop[] newArray(int size) {
            return new Tabletop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getIdentificacion());
        parcel.writeString(getNombre());
        parcel.writeInt(getYear());
        parcel.writeString(getPublisher());

    }

}
