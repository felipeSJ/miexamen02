package cr.ac.ucr.ecci.cql.miexamen02;

import java.util.List;

public interface TabletopInteractor {
    interface OnFinishedListener {
        void onFinished(List<Tabletop> items);
    }

    void getItems(OnFinishedListener listener);
}
