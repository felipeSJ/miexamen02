package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static org.hamcrest.Matchers.anything;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)

public class PruebasInstrumentadas {
    //Variables estáticas para comparar el primer elemento de la lista
    public static final String TEST_STRING_ID = "TN001";
    public static final String TEST_STRING_NOMBRE = "Catan";
    public static final String TEST_STRING_AÑO = "1995";
    public static final String TEST_STRING_PUBLISHER = "Kosmos";


    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //Test para verificar la lista es mostrada al usuario
    @Test
    public void listViewDisplayed() {
        onView(withId(R.id.list)).check(matches(isDisplayed()));
    }

    //Test para verificar que al clickear el primer elemento de la lista la información mostrada es la correcta
    @Test
    public void testClickItem() {
        onData(anything()).inAdapterView(withId(R.id.list)).atPosition(0).perform(click());
        onView(withId(R.id.itemId)).check(matches(withText(TEST_STRING_ID)));
        onView(withId(R.id.itemNombre)).check(matches(withText(TEST_STRING_NOMBRE)));
        onView(withId(R.id.itemAño)).check(matches(withText(TEST_STRING_AÑO)));
        onView(withId(R.id.itemPublisher)).check(matches(withText(TEST_STRING_PUBLISHER)));
    }

}
